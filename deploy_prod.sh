umask 0000
cd /var/www

git pull
mkdir $1
echo "Created dir my_project"
tar -xzvf project.tar.gz -C $1 && echo "Unzip"
ln -s /var/www/$1 /var/www/my_project
cd my_project
composer install
php bin/console cache:clear
php bin/console cache:warmup
echo "Success!"
