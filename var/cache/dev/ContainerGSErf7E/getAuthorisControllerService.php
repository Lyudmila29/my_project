<?php

namespace ContainerGSErf7E;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getAuthorisControllerService extends App_KernelDevDebugContainer
{
    /**
     * Gets the public 'App\Controller\AuthorisController' shared autowired service.
     *
     * @return \App\Controller\AuthorisController
     */
    public static function do($container, $lazyLoad = true)
    {
        include_once \dirname(__DIR__, 4).'/src/Controller/AuthorisController.php';

        return $container->services['App\\Controller\\AuthorisController'] = new \App\Controller\AuthorisController();
    }
}
