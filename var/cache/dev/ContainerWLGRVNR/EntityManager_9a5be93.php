<?php

namespace ContainerWLGRVNR;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder8b81e = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer6a57a = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties15ce5 = [
        
    ];

    public function getConnection()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getConnection', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getMetadataFactory', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getExpressionBuilder', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'beginTransaction', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getCache', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getCache();
    }

    public function transactional($func)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'transactional', array('func' => $func), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->transactional($func);
    }

    public function commit()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'commit', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->commit();
    }

    public function rollback()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'rollback', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getClassMetadata', array('className' => $className), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'createQuery', array('dql' => $dql), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'createNamedQuery', array('name' => $name), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'createQueryBuilder', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'flush', array('entity' => $entity), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'clear', array('entityName' => $entityName), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->clear($entityName);
    }

    public function close()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'close', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->close();
    }

    public function persist($entity)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'persist', array('entity' => $entity), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'remove', array('entity' => $entity), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'refresh', array('entity' => $entity), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'detach', array('entity' => $entity), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'merge', array('entity' => $entity), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getRepository', array('entityName' => $entityName), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'contains', array('entity' => $entity), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getEventManager', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getConfiguration', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'isOpen', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getUnitOfWork', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getProxyFactory', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'initializeObject', array('obj' => $obj), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'getFilters', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'isFiltersStateClean', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'hasFilters', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return $this->valueHolder8b81e->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer6a57a = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder8b81e) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder8b81e = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder8b81e->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, '__get', ['name' => $name], $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        if (isset(self::$publicProperties15ce5[$name])) {
            return $this->valueHolder8b81e->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8b81e;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder8b81e;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8b81e;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder8b81e;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, '__isset', array('name' => $name), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8b81e;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder8b81e;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, '__unset', array('name' => $name), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8b81e;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder8b81e;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, '__clone', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        $this->valueHolder8b81e = clone $this->valueHolder8b81e;
    }

    public function __sleep()
    {
        $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, '__sleep', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;

        return array('valueHolder8b81e');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer6a57a = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer6a57a;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer6a57a && ($this->initializer6a57a->__invoke($valueHolder8b81e, $this, 'initializeProxy', array(), $this->initializer6a57a) || 1) && $this->valueHolder8b81e = $valueHolder8b81e;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder8b81e;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder8b81e;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
