<?php

// This file has been auto-generated by the Symfony Routing Component.

return [
    'admin_index' => [[], ['_controller' => 'App\\Controller\\AdminController::index'], [], [['text', '/admin/']], [], []],
    'admin_new' => [[], ['_controller' => 'App\\Controller\\AdminController::new'], [], [['text', '/admin/new']], [], []],
    'admin_show' => [['id'], ['_controller' => 'App\\Controller\\AdminController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin']], [], []],
    'admin_edit' => [['id'], ['_controller' => 'App\\Controller\\AdminController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/admin']], [], []],
    'admin_delete' => [['id'], ['_controller' => 'App\\Controller\\AdminController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/admin']], [], []],
    'authorisation' => [[], ['_controller' => 'App\\Controller\\AuthorisationController::index'], [], [['text', '/authorisation']], [], []],
    'department_index' => [[], ['_controller' => 'App\\Controller\\DepartmentController::index'], [], [['text', '/department/']], [], []],
    'department_new' => [[], ['_controller' => 'App\\Controller\\DepartmentController::new'], [], [['text', '/department/new']], [], []],
    'department_show' => [['id'], ['_controller' => 'App\\Controller\\DepartmentController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/department']], [], []],
    'department_edit' => [['id'], ['_controller' => 'App\\Controller\\DepartmentController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/department']], [], []],
    'department_delete' => [['id'], ['_controller' => 'App\\Controller\\DepartmentController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/department']], [], []],
    'employee_index' => [[], ['_controller' => 'App\\Controller\\EmployeeController::index'], [], [['text', '/employee/']], [], []],
    'employee_new' => [[], ['_controller' => 'App\\Controller\\EmployeeController::new'], [], [['text', '/employee/new']], [], []],
    'employee_show' => [['id'], ['_controller' => 'App\\Controller\\EmployeeController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/employee']], [], []],
    'employee_edit' => [['id'], ['_controller' => 'App\\Controller\\EmployeeController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/employee']], [], []],
    'employee_delete' => [['id'], ['_controller' => 'App\\Controller\\EmployeeController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/employee']], [], []],
    'product_index' => [[], ['_controller' => 'App\\Controller\\ProductController::index'], [], [['text', '/product/']], [], []],
    'product_new' => [[], ['_controller' => 'App\\Controller\\ProductController::new'], [], [['text', '/product/new']], [], []],
    'product_show' => [['id'], ['_controller' => 'App\\Controller\\ProductController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/product']], [], []],
    'product_edit' => [['id'], ['_controller' => 'App\\Controller\\ProductController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/product']], [], []],
    'product_delete' => [['id'], ['_controller' => 'App\\Controller\\ProductController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/product']], [], []],
    'sale_index' => [[], ['_controller' => 'App\\Controller\\SaleController::index'], [], [['text', '/sale/']], [], []],
    'sale_new' => [[], ['_controller' => 'App\\Controller\\SaleController::new'], [], [['text', '/sale/new']], [], []],
    'sale_show' => [['id'], ['_controller' => 'App\\Controller\\SaleController::show'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/sale']], [], []],
    'sale_edit' => [['id'], ['_controller' => 'App\\Controller\\SaleController::edit'], [], [['text', '/edit'], ['variable', '/', '[^/]++', 'id', true], ['text', '/sale']], [], []],
    'sale_delete' => [['id'], ['_controller' => 'App\\Controller\\SaleController::delete'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/sale']], [], []],
    'app_login' => [[], ['_controller' => 'App\\Controller\\SecurityController::loginAction'], [], [['text', '/login']], [], []],
    'app_logout' => [[], ['_controller' => 'App\\Controller\\SecurityController::logout'], [], [['text', '/logout']], [], []],
    'api_entrypoint' => [['index', '_format'], ['_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index' => 'index'], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', 'index', 'index', true], ['text', '/api']], [], []],
    'api_doc' => [['_format'], ['_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], [], [['variable', '.', '[^/]++', '_format', true], ['text', '/api/docs']], [], []],
    'api_jsonld_context' => [['shortName', '_format'], ['_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName' => '.+'], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '.+', 'shortName', true], ['text', '/api/contexts']], [], []],
    'api_sales_get_collection' => [['_format'], ['_controller' => 'api_platform.action.get_collection', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_collection_operation_name' => 'get'], [], [['variable', '.', '[^/]++', '_format', true], ['text', '/api/sales']], [], []],
    'api_sales_post_collection' => [['_format'], ['_controller' => 'api_platform.action.post_collection', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_collection_operation_name' => 'post'], [], [['variable', '.', '[^/]++', '_format', true], ['text', '/api/sales']], [], []],
    'api_sales_get_item' => [['id', '_format'], ['_controller' => 'api_platform.action.get_item', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_item_operation_name' => 'get'], [], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '[^/\\.]++', 'id', true], ['text', '/api/sales']], [], []],
    'api_sales_delete_item' => [['id', '_format'], ['_controller' => 'api_platform.action.delete_item', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_item_operation_name' => 'delete'], [], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '[^/\\.]++', 'id', true], ['text', '/api/sales']], [], []],
    'api_sales_put_item' => [['id', '_format'], ['_controller' => 'api_platform.action.put_item', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_item_operation_name' => 'put'], [], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '[^/\\.]++', 'id', true], ['text', '/api/sales']], [], []],
    'api_sales_patch_item' => [['id', '_format'], ['_controller' => 'api_platform.action.patch_item', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_item_operation_name' => 'patch'], [], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '[^/\\.]++', 'id', true], ['text', '/api/sales']], [], []],
    'authentication_token' => [[], [], [], [['text', '/authentication_token']], [], []],
];
