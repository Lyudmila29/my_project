<?php

namespace Container3HYH2Rk;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getArgumentResolver_ServiceService extends App_KernelProdContainer
{
    /*
     * Gets the private 'argument_resolver.service' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['argument_resolver.service'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'App\\Controller\\AdminController::delete' => ['privates', '.service_locator.6eVqP1n', 'get_ServiceLocator_6eVqP1nService', true],
            'App\\Controller\\AdminController::edit' => ['privates', '.service_locator.6eVqP1n', 'get_ServiceLocator_6eVqP1nService', true],
            'App\\Controller\\AdminController::index' => ['privates', '.service_locator.GqNWkCX', 'get_ServiceLocator_GqNWkCXService', true],
            'App\\Controller\\AdminController::show' => ['privates', '.service_locator.6eVqP1n', 'get_ServiceLocator_6eVqP1nService', true],
            'App\\Controller\\DepartmentController::delete' => ['privates', '.service_locator.s8fCHH7', 'get_ServiceLocator_S8fCHH7Service', true],
            'App\\Controller\\DepartmentController::edit' => ['privates', '.service_locator.s8fCHH7', 'get_ServiceLocator_S8fCHH7Service', true],
            'App\\Controller\\DepartmentController::index' => ['privates', '.service_locator.Lx3Oc0p', 'get_ServiceLocator_Lx3Oc0pService', true],
            'App\\Controller\\DepartmentController::show' => ['privates', '.service_locator.s8fCHH7', 'get_ServiceLocator_S8fCHH7Service', true],
            'App\\Controller\\EmployeeController::delete' => ['privates', '.service_locator.s8htEgk', 'get_ServiceLocator_S8htEgkService', true],
            'App\\Controller\\EmployeeController::edit' => ['privates', '.service_locator.s8htEgk', 'get_ServiceLocator_S8htEgkService', true],
            'App\\Controller\\EmployeeController::index' => ['privates', '.service_locator.Q2hdW62', 'get_ServiceLocator_Q2hdW62Service', true],
            'App\\Controller\\EmployeeController::show' => ['privates', '.service_locator.s8htEgk', 'get_ServiceLocator_S8htEgkService', true],
            'App\\Controller\\ProductController::delete' => ['privates', '.service_locator.Rc9uH46', 'get_ServiceLocator_Rc9uH46Service', true],
            'App\\Controller\\ProductController::edit' => ['privates', '.service_locator.Rc9uH46', 'get_ServiceLocator_Rc9uH46Service', true],
            'App\\Controller\\ProductController::index' => ['privates', '.service_locator.JnHMbEU', 'get_ServiceLocator_JnHMbEUService', true],
            'App\\Controller\\ProductController::show' => ['privates', '.service_locator.Rc9uH46', 'get_ServiceLocator_Rc9uH46Service', true],
            'App\\Controller\\SaleController::delete' => ['privates', '.service_locator.Sb3wrzA', 'get_ServiceLocator_Sb3wrzAService', true],
            'App\\Controller\\SaleController::edit' => ['privates', '.service_locator.Sb3wrzA', 'get_ServiceLocator_Sb3wrzAService', true],
            'App\\Controller\\SaleController::index' => ['privates', '.service_locator.atLGLQt', 'get_ServiceLocator_AtLGLQtService', true],
            'App\\Controller\\SaleController::show' => ['privates', '.service_locator.Sb3wrzA', 'get_ServiceLocator_Sb3wrzAService', true],
            'App\\Controller\\SecurityController::loginAction' => ['privates', '.service_locator.xA8Fw_.', 'get_ServiceLocator_XA8Fw_Service', true],
            'App\\Kernel::loadRoutes' => ['privates', '.service_locator.C9JCBPC', 'get_ServiceLocator_C9JCBPCService', true],
            'App\\Kernel::registerContainerConfiguration' => ['privates', '.service_locator.C9JCBPC', 'get_ServiceLocator_C9JCBPCService', true],
            'App\\Kernel::terminate' => ['privates', '.service_locator.beq5mCo', 'get_ServiceLocator_Beq5mCoService', true],
            'kernel::loadRoutes' => ['privates', '.service_locator.C9JCBPC', 'get_ServiceLocator_C9JCBPCService', true],
            'kernel::registerContainerConfiguration' => ['privates', '.service_locator.C9JCBPC', 'get_ServiceLocator_C9JCBPCService', true],
            'kernel::terminate' => ['privates', '.service_locator.beq5mCo', 'get_ServiceLocator_Beq5mCoService', true],
            'App\\Controller\\AdminController:delete' => ['privates', '.service_locator.6eVqP1n', 'get_ServiceLocator_6eVqP1nService', true],
            'App\\Controller\\AdminController:edit' => ['privates', '.service_locator.6eVqP1n', 'get_ServiceLocator_6eVqP1nService', true],
            'App\\Controller\\AdminController:index' => ['privates', '.service_locator.GqNWkCX', 'get_ServiceLocator_GqNWkCXService', true],
            'App\\Controller\\AdminController:show' => ['privates', '.service_locator.6eVqP1n', 'get_ServiceLocator_6eVqP1nService', true],
            'App\\Controller\\DepartmentController:delete' => ['privates', '.service_locator.s8fCHH7', 'get_ServiceLocator_S8fCHH7Service', true],
            'App\\Controller\\DepartmentController:edit' => ['privates', '.service_locator.s8fCHH7', 'get_ServiceLocator_S8fCHH7Service', true],
            'App\\Controller\\DepartmentController:index' => ['privates', '.service_locator.Lx3Oc0p', 'get_ServiceLocator_Lx3Oc0pService', true],
            'App\\Controller\\DepartmentController:show' => ['privates', '.service_locator.s8fCHH7', 'get_ServiceLocator_S8fCHH7Service', true],
            'App\\Controller\\EmployeeController:delete' => ['privates', '.service_locator.s8htEgk', 'get_ServiceLocator_S8htEgkService', true],
            'App\\Controller\\EmployeeController:edit' => ['privates', '.service_locator.s8htEgk', 'get_ServiceLocator_S8htEgkService', true],
            'App\\Controller\\EmployeeController:index' => ['privates', '.service_locator.Q2hdW62', 'get_ServiceLocator_Q2hdW62Service', true],
            'App\\Controller\\EmployeeController:show' => ['privates', '.service_locator.s8htEgk', 'get_ServiceLocator_S8htEgkService', true],
            'App\\Controller\\ProductController:delete' => ['privates', '.service_locator.Rc9uH46', 'get_ServiceLocator_Rc9uH46Service', true],
            'App\\Controller\\ProductController:edit' => ['privates', '.service_locator.Rc9uH46', 'get_ServiceLocator_Rc9uH46Service', true],
            'App\\Controller\\ProductController:index' => ['privates', '.service_locator.JnHMbEU', 'get_ServiceLocator_JnHMbEUService', true],
            'App\\Controller\\ProductController:show' => ['privates', '.service_locator.Rc9uH46', 'get_ServiceLocator_Rc9uH46Service', true],
            'App\\Controller\\SaleController:delete' => ['privates', '.service_locator.Sb3wrzA', 'get_ServiceLocator_Sb3wrzAService', true],
            'App\\Controller\\SaleController:edit' => ['privates', '.service_locator.Sb3wrzA', 'get_ServiceLocator_Sb3wrzAService', true],
            'App\\Controller\\SaleController:index' => ['privates', '.service_locator.atLGLQt', 'get_ServiceLocator_AtLGLQtService', true],
            'App\\Controller\\SaleController:show' => ['privates', '.service_locator.Sb3wrzA', 'get_ServiceLocator_Sb3wrzAService', true],
            'App\\Controller\\SecurityController:loginAction' => ['privates', '.service_locator.xA8Fw_.', 'get_ServiceLocator_XA8Fw_Service', true],
            'kernel:loadRoutes' => ['privates', '.service_locator.C9JCBPC', 'get_ServiceLocator_C9JCBPCService', true],
            'kernel:registerContainerConfiguration' => ['privates', '.service_locator.C9JCBPC', 'get_ServiceLocator_C9JCBPCService', true],
            'kernel:terminate' => ['privates', '.service_locator.beq5mCo', 'get_ServiceLocator_Beq5mCoService', true],
        ], [
            'App\\Controller\\AdminController::delete' => '?',
            'App\\Controller\\AdminController::edit' => '?',
            'App\\Controller\\AdminController::index' => '?',
            'App\\Controller\\AdminController::show' => '?',
            'App\\Controller\\DepartmentController::delete' => '?',
            'App\\Controller\\DepartmentController::edit' => '?',
            'App\\Controller\\DepartmentController::index' => '?',
            'App\\Controller\\DepartmentController::show' => '?',
            'App\\Controller\\EmployeeController::delete' => '?',
            'App\\Controller\\EmployeeController::edit' => '?',
            'App\\Controller\\EmployeeController::index' => '?',
            'App\\Controller\\EmployeeController::show' => '?',
            'App\\Controller\\ProductController::delete' => '?',
            'App\\Controller\\ProductController::edit' => '?',
            'App\\Controller\\ProductController::index' => '?',
            'App\\Controller\\ProductController::show' => '?',
            'App\\Controller\\SaleController::delete' => '?',
            'App\\Controller\\SaleController::edit' => '?',
            'App\\Controller\\SaleController::index' => '?',
            'App\\Controller\\SaleController::show' => '?',
            'App\\Controller\\SecurityController::loginAction' => '?',
            'App\\Kernel::loadRoutes' => '?',
            'App\\Kernel::registerContainerConfiguration' => '?',
            'App\\Kernel::terminate' => '?',
            'kernel::loadRoutes' => '?',
            'kernel::registerContainerConfiguration' => '?',
            'kernel::terminate' => '?',
            'App\\Controller\\AdminController:delete' => '?',
            'App\\Controller\\AdminController:edit' => '?',
            'App\\Controller\\AdminController:index' => '?',
            'App\\Controller\\AdminController:show' => '?',
            'App\\Controller\\DepartmentController:delete' => '?',
            'App\\Controller\\DepartmentController:edit' => '?',
            'App\\Controller\\DepartmentController:index' => '?',
            'App\\Controller\\DepartmentController:show' => '?',
            'App\\Controller\\EmployeeController:delete' => '?',
            'App\\Controller\\EmployeeController:edit' => '?',
            'App\\Controller\\EmployeeController:index' => '?',
            'App\\Controller\\EmployeeController:show' => '?',
            'App\\Controller\\ProductController:delete' => '?',
            'App\\Controller\\ProductController:edit' => '?',
            'App\\Controller\\ProductController:index' => '?',
            'App\\Controller\\ProductController:show' => '?',
            'App\\Controller\\SaleController:delete' => '?',
            'App\\Controller\\SaleController:edit' => '?',
            'App\\Controller\\SaleController:index' => '?',
            'App\\Controller\\SaleController:show' => '?',
            'App\\Controller\\SecurityController:loginAction' => '?',
            'kernel:loadRoutes' => '?',
            'kernel:registerContainerConfiguration' => '?',
            'kernel:terminate' => '?',
        ]));
    }
}
