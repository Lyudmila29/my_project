<?php

namespace Container3HYH2Rk;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/*
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getSaleRepositoryService extends App_KernelProdContainer
{
    /*
     * Gets the private 'App\Repository\SaleRepository' shared autowired service.
     *
     * @return \App\Repository\SaleRepository
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['App\\Repository\\SaleRepository'] = new \App\Repository\SaleRepository(($container->services['doctrine'] ?? $container->getDoctrineService()));
    }
}
