<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/admin' => [[['_route' => 'admin_index', '_controller' => 'App\\Controller\\AdminController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/new' => [[['_route' => 'admin_new', '_controller' => 'App\\Controller\\AdminController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/authorisation' => [[['_route' => 'authorisation', '_controller' => 'App\\Controller\\AuthorisationController::index'], null, null, null, false, false, null]],
        '/department' => [[['_route' => 'department_index', '_controller' => 'App\\Controller\\DepartmentController::index'], null, ['GET' => 0], null, true, false, null]],
        '/department/new' => [[['_route' => 'department_new', '_controller' => 'App\\Controller\\DepartmentController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/employee' => [[['_route' => 'employee_index', '_controller' => 'App\\Controller\\EmployeeController::index'], null, ['GET' => 0], null, true, false, null]],
        '/employee/new' => [[['_route' => 'employee_new', '_controller' => 'App\\Controller\\EmployeeController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/product' => [[['_route' => 'product_index', '_controller' => 'App\\Controller\\ProductController::index'], null, ['GET' => 0], null, true, false, null]],
        '/product/new' => [[['_route' => 'product_new', '_controller' => 'App\\Controller\\ProductController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/sale' => [[['_route' => 'sale_index', '_controller' => 'App\\Controller\\SaleController::index'], null, ['GET' => 0], null, true, false, null]],
        '/sale/new' => [[['_route' => 'sale_new', '_controller' => 'App\\Controller\\SaleController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::loginAction'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/authentication_token' => [[['_route' => 'authentication_token'], null, ['POST' => 0], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/a(?'
                    .'|dmin/([^/]++)(?'
                        .'|(*:28)'
                        .'|/edit(*:40)'
                        .'|(*:47)'
                    .')'
                    .'|pi(?'
                        .'|(?:/(index)(?:\\.([^/]++))?)?(*:88)'
                        .'|/(?'
                            .'|docs(?:\\.([^/]++))?(*:118)'
                            .'|contexts/(.+)(?:\\.([^/]++))?(*:154)'
                            .'|sales(?'
                                .'|(?:\\.([^/]++))?(?'
                                    .'|(*:188)'
                                .')'
                                .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                    .'|(*:226)'
                                .')'
                            .')'
                        .')'
                    .')'
                .')'
                .'|/department/([^/]++)(?'
                    .'|(*:262)'
                    .'|/edit(*:275)'
                    .'|(*:283)'
                .')'
                .'|/employee/([^/]++)(?'
                    .'|(*:313)'
                    .'|/edit(*:326)'
                    .'|(*:334)'
                .')'
                .'|/product/([^/]++)(?'
                    .'|(*:363)'
                    .'|/edit(*:376)'
                    .'|(*:384)'
                .')'
                .'|/sale/([^/]++)(?'
                    .'|(*:410)'
                    .'|/edit(*:423)'
                    .'|(*:431)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        28 => [[['_route' => 'admin_show', '_controller' => 'App\\Controller\\AdminController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        40 => [[['_route' => 'admin_edit', '_controller' => 'App\\Controller\\AdminController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        47 => [[['_route' => 'admin_delete', '_controller' => 'App\\Controller\\AdminController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        88 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index', '_format'], null, null, false, true, null]],
        118 => [[['_route' => 'api_doc', '_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], ['_format'], null, null, false, true, null]],
        154 => [[['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName', '_format'], null, null, false, true, null]],
        188 => [
            [['_route' => 'api_sales_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_collection_operation_name' => 'get'], ['_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_sales_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_collection_operation_name' => 'post'], ['_format'], ['POST' => 0], null, false, true, null],
        ],
        226 => [
            [['_route' => 'api_sales_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_item_operation_name' => 'get'], ['id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_sales_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_item_operation_name' => 'delete'], ['id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_sales_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_item_operation_name' => 'put'], ['id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'api_sales_patch_item', '_controller' => 'api_platform.action.patch_item', '_format' => null, '_api_resource_class' => 'App\\Entity\\Sale', '_api_item_operation_name' => 'patch'], ['id', '_format'], ['PATCH' => 0], null, false, true, null],
        ],
        262 => [[['_route' => 'department_show', '_controller' => 'App\\Controller\\DepartmentController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        275 => [[['_route' => 'department_edit', '_controller' => 'App\\Controller\\DepartmentController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        283 => [[['_route' => 'department_delete', '_controller' => 'App\\Controller\\DepartmentController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        313 => [[['_route' => 'employee_show', '_controller' => 'App\\Controller\\EmployeeController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        326 => [[['_route' => 'employee_edit', '_controller' => 'App\\Controller\\EmployeeController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        334 => [[['_route' => 'employee_delete', '_controller' => 'App\\Controller\\EmployeeController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        363 => [[['_route' => 'product_show', '_controller' => 'App\\Controller\\ProductController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        376 => [[['_route' => 'product_edit', '_controller' => 'App\\Controller\\ProductController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        384 => [[['_route' => 'product_delete', '_controller' => 'App\\Controller\\ProductController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        410 => [[['_route' => 'sale_show', '_controller' => 'App\\Controller\\SaleController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        423 => [[['_route' => 'sale_edit', '_controller' => 'App\\Controller\\SaleController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        431 => [
            [['_route' => 'sale_delete', '_controller' => 'App\\Controller\\SaleController::delete'], ['id'], ['DELETE' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
